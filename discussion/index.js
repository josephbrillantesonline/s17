// [SECTION] INTRODUCTION TO ARRAYS

	// practice: create/declare multiple variables



  let student1= 'Martin'; 
  let student2= 'Maria' ;
  let student3= 'john' ;
  let student4= 'Ernie' ;
  let student5= 'bert' ;
  let student6= 'Zeke' ;


  // store the ff values inside a single container
  // to declare array => we simple use an 'array literal' or Square bracket ['']
  // use '=' => assignment operator to repackage the structure inside the variable
  // be careful when selecting variable names. (reserve i.e class)
  let batch = ['Martin', 'Maria', "john", 'Ernie', 'bert', 'Zeke']; 
  console.log(batch);

  // create an array that will contain diff compuerBrands
  let computerBrands = ['Asus', 'Dell', 'Apple', 'Acer', 'Toshiba', 'Fujitsu'];
  // '' or "" => both are used to declare a string data type in JS
  // [SIDE LESSON]
  // => 'apple' === "apple"
  // 'apple' correct
   // "apple" correct
   // 'apple" incorrect

   // HOW TO USE?
   // => Use case of the data

   // 1. if ur going to use quotaions, dialogs inside a string.
   // if you will use single quotations when declaring such values like below. it will result to prematurely end the statement
   let message = "Using Javascript's Array Literal";
   console.log(message);

   let lastWords = '"I Shall Return" - MacArthur';
   console.log(lastWords);

   // NOTE: when selecting a correct quotation type, you can use them as an [ESCAPE TOOL] to properly declare expression within a string.

   // [ALTERNATIVE APPROACH] "\" forward slash to insert quotations.
   


  // console.log(computerBrands);
